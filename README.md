# CharManager
Up to date issues:https://mercury.filedotexe.us/filedotexe/charmanager/-/issues?scope=all&utf8=%E2%9C%93&state=closed

**CharManager** 
**Advanced plugin management**
- CharManager is an advanced plugin manager that can download plugins directly from SpigotMC (excluding premium) in game, update managed plugins automatically, download jars directly from URLs, compile maven and gradle plugins (ALPHA) and manage downloads from Jenkins servers. 
**Commands**

- /psearch (plugin) – searches for a plugin on spigot | char.psearch

- /pinstall (id) – installs a plugin with its given id, this can be grabbed from a URL or by using the psearch command | char.pinstall

- /pmanage – View and manage plugins | char.pmanage

- /pdownload <jar url> - Downloads jars directly to the plugins folder | char.pdownload

- /padd <jar url> - Manually adds a jar to the managed plugins, this is recommended for updating jars straight from a CI such as Jenkins, GitLab or TeamCity. | char.padd

- /pcompile (m/g) <git url> - Compile maven or gradle plugins directly on your server (this is in alpha) | char.pcompile

- /preload (plugin) – reloads specified plugin | char.preload

- /pload (plugin) – manually loads jar file in (if auto load fails) | char.pload

- /punload (plugin) – unloads specified plugin | char.punload
**Support**
OS	Manager	Compiler
Linux, Unix, MacOS	Supported 	Supported*
Windows	Supported**	Supported, Beta***
*Requires git and unzip to be installed. 
On Debian/Ubuntu this can be done with sudo apt-get install unzip git
On MacOS this can be done with brew install git
**Due to a Windows limitation, we cannot delete old jars, updated plugins will be thrown into an updated-plugins folder inside the CharManager plugin folder.
***Compiling on Windows is currently limited, as of now only maven is supported
